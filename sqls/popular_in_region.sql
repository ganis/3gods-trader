SELECT mi.name, sum(mh.order_count) as "Orders", sum(mh.volume) as "Volume"
FROM market_item mi
JOIN market_markethistory mh ON mi.eve_id = mh.type_eve_id
JOIN market_region mr on mh.region_eve_id = mr.eve_id
WHERE mh.date > current_date - interval '30 days'
AND mr.name = 'Pochven'
GROUP BY mi.name;
