SELECT
    t1.eve_id,
    t1.name,
    t1.jita - t1.amarr
FROM (
    SELECT
        mi.eve_id AS "eve_id",
        mi.name AS "name",
        -- Min Jita sell
        (SELECT min(jita.price) FROM market_marketorder jita WHERE jita.location_eve_id = 60003760 AND jita.type_eve_id = mi.eve_id AND jita.is_buy_order = False) as "jita",
        -- Max Dodixie buy
        (SELECT max(dodixie.price) FROM market_marketorder dodixie WHERE dodixie.location_eve_id I=60011866 AND dodixie.type_eve_id = mi.eve_id AND dodixie.is_buy_order = True) as "dodixie"
    FROM market_item mi) t1
WHERE t1.jita is not NULL AND t1.dodixie is not NULL
ORDER by 3 DESC;