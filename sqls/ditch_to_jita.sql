SELECT item.name, item.volume, max(jita.price) as "jita buy", min(ditch.price) as "Ditch sell", max(jita.price)/min(ditch.price) as "ratio", max(jita.price) - min(ditch.price) as "Profit?", sum(jita.volume_remain) as "jita wants", sum(ditch.volume_remain) as "Ditch has"
FROM market_item item
JOIN market_marketorder jita ON jita.type_eve_id = item.eve_id AND jita.location_eve_id IN (60003760, 1028858195912) AND jita.is_buy_order = true
JOIN market_marketorder ditch ON ditch.type_eve_id = item.eve_id AND ditch.location_eve_id = 1031787606461 AND ditch.is_buy_order = false
GROUP BY item.name, item.volume ORDER by 5 desc;