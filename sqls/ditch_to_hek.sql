SELECT item.name, item.volume,
       max(hek.price) as "hek buy",
       min(ditch.price) as "Ditch sell",
       max(hek.price)/min(ditch.price) as "ratio",
       (max(hek.price) - min(ditch.price))*least((57000/item.volume), sum(ditch.volume_remain), sum(hek.volume_remain)) as "Profit?",
       sum(hek.volume_remain) as "hek wants",
       sum(ditch.volume_remain) as "Ditch has"
FROM market_item item
JOIN market_marketorder hek ON hek.type_eve_id = item.eve_id AND hek.location_eve_id = 60005686 AND hek.is_buy_order = true
JOIN market_marketorder ditch ON ditch.type_eve_id = item.eve_id AND ditch.location_eve_id = 1031787606461 AND ditch.is_buy_order = false
GROUP BY item.name, item.volume ORDER by 5 desc;