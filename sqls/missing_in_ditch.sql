SELECT item.eve_id, item.name, item.volume,
       sum(mh.volume) as "Ditch trade volume",
       sum(mh.order_count) as "Ditch orders",
       avg(mh.average) as "Ditch price",
       (SELECT min(mo.price) FROM market_marketorder mo WHERE mo.is_buy_order = false AND mo.type_eve_id = item.eve_id AND mo.location_eve_id in (60003760, 1028858195912)) as "Jita price",
       avg(mh.average)/(SELECT min(mo.price) FROM market_marketorder mo WHERE mo.is_buy_order = false AND mo.type_eve_id = item.eve_id AND mo.location_eve_id in (60003760, 1028858195912)) as "Ratio"
FROM market_item item
 JOIN market_markethistory mh on item.eve_id = mh.type_eve_id AND mh.region_eve_id = 10000009
WHERE
    NOT EXISTS (SELECT * FROM market_marketorder mo WHERE mo.type_eve_id = item.eve_id AND mo.location_eve_id = 1031787606461)
    AND EXISTS (SELECT * FROM market_marketorder mo WHERE mo.type_eve_id = item.eve_id AND mo.location_eve_id in (60003760, 1028858195912) and mo.is_buy_order=false)
    AND mh.date > current_date - interval '90 days'
GROUP BY item.name, item.eve_id, item.volume
ORDER BY 8 DESC;