CREATE MATERIALIZED VIEW week_prices AS
SELECT
    item.eve_id,
    item.name,
    item.volume,
    (SELECT avg(market_markethistory.average) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000060
                         AND date >= current_date - interval '7 days') as "delve_average",
    (SELECT min(market_markethistory.lowest) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000060
                         AND date >= current_date - interval '7 days') as "delve_min",
    (SELECT max(market_markethistory.highest) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000060
                         AND date >= current_date - interval '7 days') as "delve_max",
    (SELECT sum(market_markethistory.volume) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000060
                         AND date >= current_date - interval '7 days') as "delve_volume",
    (SELECT sum(market_markethistory.order_count) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000060
                         AND date >= current_date - interval '7 days') as "delve_order_count",
    (SELECT avg(market_markethistory.average) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000002
                         AND date >= current_date - interval '7 days') as "forge_average",
    (SELECT min(market_markethistory.lowest) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000002
                         AND date >= current_date - interval '7 days') as "forge_min",
    (SELECT max(market_markethistory.highest) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000002
                         AND date >= current_date - interval '7 days') as "forge_max",
    (SELECT sum(market_markethistory.volume) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000002
                         AND date >= current_date - interval '7 days') as "forge_volume",
    (SELECT sum(market_markethistory.order_count) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000002
                         AND date >= current_date - interval '7 days') as "forge_order_count",
    (SELECT avg(market_markethistory.average) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000042
                         AND date >= current_date - interval '7 days') as "metropolis_average",
    (SELECT min(market_markethistory.lowest) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000042
                         AND date >= current_date - interval '7 days') as "metropolis_min",
    (SELECT max(market_markethistory.highest) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000042
                         AND date >= current_date - interval '7 days') as "metropolis_max",
    (SELECT sum(market_markethistory.volume) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000042
                         AND date >= current_date - interval '7 days') as "metropolis_volume",
    (SELECT sum(market_markethistory.order_count) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000042
                         AND date >= current_date - interval '7 days') as "metropolis_order_count",
    (SELECT avg(market_markethistory.average) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000032
                         AND date >= current_date - interval '7 days') as "sinq_laison_average",
    (SELECT min(market_markethistory.lowest) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000032
                         AND date >= current_date - interval '7 days') as "sinq_laison_min",
    (SELECT max(market_markethistory.highest) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000032
                         AND date >= current_date - interval '7 days') as "sinq_laison_max",
    (SELECT sum(market_markethistory.volume) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000032
                         AND date >= current_date - interval '7 days') as "sinq_laison_volume",
    (SELECT sum(market_markethistory.order_count) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000032
                         AND date >= current_date - interval '7 days') as "sinq_laison_order_count",
    (SELECT avg(market_markethistory.average) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000030
                         AND date >= current_date - interval '7 days') as "heimatar_average",
    (SELECT min(market_markethistory.lowest) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000030
                         AND date >= current_date - interval '7 days') as "heimatar_min",
    (SELECT max(market_markethistory.highest) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000030
                         AND date >= current_date - interval '7 days') as "heimatar_max",
    (SELECT sum(market_markethistory.volume) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000030
                         AND date >= current_date - interval '7 days') as "heimatar_volume",
    (SELECT sum(market_markethistory.order_count) FROM market_markethistory
                         WHERE market_markethistory.type_eve_id = item.eve_id
                         AND market_markethistory.region_eve_id = 10000030
                         AND date >= current_date - interval '7 days') as "heimatar_order_count"
FROM market_item item;

CREATE UNIQUE INDEX ON week_prices (eve_id);

REFRESH MATERIALIZED VIEW week_prices;