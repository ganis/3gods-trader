CREATE MATERIALIZED VIEW current_prices AS
SELECT
    item.eve_id,
    item.name,
    item.volume,
    (SELECT max(price) FROM market_marketorder
                       WHERE market_marketorder.type_eve_id = item.eve_id
                         AND market_marketorder.location_eve_id = 60003760
                         AND market_marketorder.is_buy_order = true) as "jita_buy",
    (SELECT min(price) FROM market_marketorder
                       WHERE market_marketorder.type_eve_id = item.eve_id
                         AND market_marketorder.location_eve_id = 60003760
                         AND market_marketorder.is_buy_order = false) as "jita_sell",
    (SELECT max(price) FROM market_marketorder
                       WHERE market_marketorder.type_eve_id = item.eve_id
                         AND market_marketorder.location_eve_id = 60008494
                         AND market_marketorder.is_buy_order = true) as "amarr_buy",
    (SELECT min(price) FROM market_marketorder
                       WHERE market_marketorder.type_eve_id = item.eve_id
                         AND market_marketorder.location_eve_id = 60008494
                         AND market_marketorder.is_buy_order = false) as "amarr_sell",
    (SELECT max(price) FROM market_marketorder
                       WHERE market_marketorder.type_eve_id = item.eve_id
                         AND market_marketorder.location_eve_id = 60011866
                         AND market_marketorder.is_buy_order = true) as "dodixie_buy",
    (SELECT min(price) FROM market_marketorder
                       WHERE market_marketorder.type_eve_id = item.eve_id
                         AND market_marketorder.location_eve_id = 60011866
                         AND market_marketorder.is_buy_order = false) as "dodixie_sell",
    (SELECT max(price) FROM market_marketorder
                       WHERE market_marketorder.type_eve_id = item.eve_id
                         AND market_marketorder.location_eve_id = 1028858195912
                         AND market_marketorder.is_buy_order = true) as "ttt_buy",
    (SELECT min(price) FROM market_marketorder
                       WHERE market_marketorder.type_eve_id = item.eve_id
                         AND market_marketorder.location_eve_id = 1028858195912
                         AND market_marketorder.is_buy_order = false) as "ttt_sell",
    (SELECT max(price) FROM market_marketorder
                       WHERE market_marketorder.type_eve_id = item.eve_id
                         AND market_marketorder.location_eve_id = 60005686
                         AND market_marketorder.is_buy_order = true) as "hek_buy",
    (SELECT min(price) FROM market_marketorder
                       WHERE market_marketorder.type_eve_id = item.eve_id
                         AND market_marketorder.location_eve_id = 60005686
                         AND market_marketorder.is_buy_order = false) as "hek_sell",
    (SELECT max(price) FROM market_marketorder
                       WHERE market_marketorder.type_eve_id = item.eve_id
                         AND market_marketorder.location_eve_id = 60004588
                         AND market_marketorder.is_buy_order = true) as "rens_buy",
    (SELECT min(price) FROM market_marketorder
                       WHERE market_marketorder.type_eve_id = item.eve_id
                         AND market_marketorder.location_eve_id = 60004588
                         AND market_marketorder.is_buy_order = false) as "rens_sell",
    (SELECT max(price) FROM market_marketorder
                       WHERE market_marketorder.type_eve_id = item.eve_id
                         AND market_marketorder.location_eve_id = 1030049082711
                         AND market_marketorder.is_buy_order = true) as "1dq_buy",
    (SELECT min(price) FROM market_marketorder
                       WHERE market_marketorder.type_eve_id = item.eve_id
                         AND market_marketorder.location_eve_id = 1030049082711
                         AND market_marketorder.is_buy_order = false) as "1dq_sell"
FROM market_item item;

CREATE UNIQUE INDEX ON current_prices (eve_id);

REFRESH MATERIALIZED VIEW current_prices;