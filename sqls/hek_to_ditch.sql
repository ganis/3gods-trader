SELECT item.eve_id,
       item.name,
       item.volume,
       avg(target_market.average) as "Ditch price",
       min(mo.price) as "hek price",
       (avg(target_market.average)/min(mo.price)) as "Ratio",
       (avg(target_market.average) - min(mo.price))*least(sum(target_market.volume), 57000/item.volume) as "Profit-cargo",
       (SELECT sum(hist2.volume) FROM market_markethistory hist2 WHERE hist2.region_eve_id=10000009 AND hist2.type_eve_id = item.eve_id AND hist2.date > current_date - interval '7 days') as "trade volume",
       (avg(target_market.average) - min(mo.price))*(SELECT sum(hist2.volume) FROM market_markethistory hist2 WHERE hist2.region_eve_id=10000009 AND hist2.type_eve_id = item.eve_id AND hist2.date > current_date - interval '7 days') as "Total profit?",
       (avg(target_market.average) - min(mo.price))*(SELECT sum(hist2.volume) FROM market_markethistory hist2 WHERE hist2.region_eve_id=10000009 AND hist2.type_eve_id = item.eve_id AND hist2.date > current_date - interval '7 days')/item.volume as "Total profit per m3?"
FROM market_item item
JOIN (
    SELECT hist.type_eve_id, hist.volume, hist.average
    FROM market_markethistory hist
     JOIN market_region reg ON reg.eve_id = hist.region_eve_id
     WHERE reg.name = 'Insmother' AND hist.date > current_date - interval '7 days'
) target_market ON target_market.type_eve_id = item.eve_id
JOIN market_marketorder mo on item.eve_id = mo.type_eve_id
WHERE mo.location_eve_id IN (60005686, 1031058135975) AND mo.is_buy_order = false
    AND (SELECT sum(hist2.order_count) FROM market_markethistory hist2 WHERE hist2.region_eve_id=10000009 AND hist2.type_eve_id = item.eve_id AND hist2.date > current_date - interval '7 days') > 10
GROUP BY item.eve_id, item.name, item.volume
ORDER BY 7 desc;



select * from market_region where name = 'Insmother'