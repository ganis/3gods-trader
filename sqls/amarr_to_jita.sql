-- SELECT mi.eve_id, mi.name
-- FROM market_item mi
-- JOIN market_marketorder mo_jita ON mo_jita.location_eve_id IN (60003760, 1028858195912) AND mo_jita.is_buy_order = True
-- JOIN market_marketorder mo_amarr ON mo_amarr.location_eve_id = 60008494 AND mo_amarr.is_buy_order = False
-- GROUP BY

SELECT
    t1.eve_id,
    t1.name,
    t1.jita - t1.amarr
FROM (
    SELECT
        mi.eve_id AS "eve_id",
        mi.name AS "name",
        -- Min Amarr sell
        (SELECT min(amarr.price) FROM market_marketorder amarr WHERE amarr.location_eve_id = 60008494 AND amarr.type_eve_id = mi.eve_id AND amarr.is_buy_order = False) as "amarr",
        -- Max Jita buy
        (SELECT max(jita.price) FROM market_marketorder jita WHERE jita.location_eve_id IN (60003760, 1028858195912) AND jita.type_eve_id = mi.eve_id AND jita.is_buy_order = True) as "jita"
    FROM market_item mi) t1
WHERE t1.jita is not NULL AND t1.amarr is not NULL
ORDER by 3 DESC;



