python3 manage.py download_statics regions
python3 manage.py download_statics constellations
python3 manage.py download_statics systems
python3 manage.py download_statics market_groups
python3 manage.py download_statics types

python3 manage.py download_structures all
python3 manage.py download_region all
python3 manage.py download_history all
