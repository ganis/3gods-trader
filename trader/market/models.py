import datetime
from typing import List, Set

import tqdm
from django.db import models, connection
from django.db.models import UniqueConstraint

from eve_auth.client import Client
from multiprocessing import Queue, pool, cpu_count
from queue import Empty
import re

ETagRegex = re.compile(r'W/"(\w+)"')


class EVEListedObject(models.Model):
    LIST_URL = ""
    LIST_PAGED = True
    DETAILS_URL = "Some address with a place for {id} to get details"
    etag = models.CharField(max_length=64)
    eve_id = models.IntegerField(unique=True)

    class Meta:
        abstract = True

    @classmethod
    def get_list_of_ids(cls) -> Set[int]:
        client = Client.get_client()
        ids = set()
        resp_headers = client.make_request(cls.LIST_URL, page=1, only_headers=True)
        pages = int(resp_headers.headers.get('x-pages', 1))
        for page in range(1, pages+1):
            response = client.make_request(cls.LIST_URL, page=page)
            resp_data = response.json()
            for eve_id in resp_data:
                ids.add(eve_id)
            if not (resp_data and cls.LIST_PAGED):
                break
        return ids

    @classmethod
    def update_eve_id(cls, eve_id, etag):
        client = Client.get_client()
        resp = client.make_request(cls.DETAILS_URL.format(id=eve_id), etag=etag)
        if resp == Client.Http304:
            # Nothing to change here
            return None
        try:
            obj = cls.objects.get(eve_id=eve_id)
        except cls.DoesNotExist:
            obj = cls()
        etag = ETagRegex.search(resp.headers['Etag'])
        if etag:
            etag = etag.group(1)
        else:
            etag = resp.headers['Etag']
        obj.load_dict(resp.json(), etag)
        return obj

    def load_dict(self, data: dict, etag: str):
        """
        Load data from the received JSON dict and save it with given etag.
        :param data:
        :param etag:
        :return:
        """
        pass

    @classmethod
    def reconcile(cls):
        """
        Some types of objects might need to build relations after all are created.
        :return:
        """
        pass

    @classmethod
    def get_list_of_known_ids(cls):
        return cls.objects.values_list('eve_id', 'etag')


# Create your models here.
class MarketGroup(EVEListedObject):
    LIST_URL = "https://esi.evetech.net/latest/markets/groups/"
    LIST_PAGED = False
    DETAILS_URL = "https://esi.evetech.net/latest/markets/groups/{id}/"
    name = models.CharField(max_length=128)
    parent_group_eve_id = models.IntegerField(null=True)
    full_name = models.CharField(max_length=1024)

    def __init__(self, *args, **kwargs):
        super(MarketGroup, self).__init__(*args, **kwargs)

    def __repr__(self):
        return str(self)

    def __str__(self):
        if self.parent_group_eve_id is None:
            return self.name
        else:
            parent = MarketGroup.objects.get(eve_id=self.parent_group_eve_id)
            return "{} > {}".format(parent, self.name)

    def load_dict(self, data: dict, etag: str):
        self.eve_id = data['market_group_id']
        self.name = data['name']
        self.full_name = data['name']
        self.parent_group_eve_id = data.get('parent_group_id', None)
        self.etag = etag
        # self.save()

    @classmethod
    def reconcile(cls):
        for mg in MarketGroup.objects.all():
            mg.full_name = str(mg)
            mg.save()


class Item(EVEListedObject):
    LIST_URL = "https://esi.evetech.net/latest/universe/types/"
    DETAILS_URL = "https://esi.evetech.net/latest/universe/types/{id}/"
    name = models.CharField(max_length=128, db_index=True)
    volume = models.FloatField(null=True)
    mass = models.FloatField(null=True)
    market_group_eve_id = models.IntegerField(null=True, db_index=True)
    on_market = models.BooleanField(default=True)

    def __init__(self, *args, **kwargs):
        super(Item, self).__init__(*args, **kwargs)

    def __str__(self):
        return self.name

    def __repr__(self):
        return str(self)

    def load_dict(self, data: dict, etag: str):
        self.eve_id = data['type_id']
        self.name = data['name']
        self.volume = data.get('volume', 0)
        self.mass = data.get('mass', 0)
        self.market_group_eve_id = data.get('market_group_id', None)
        self.on_market = self.market_group_eve_id is not None
        self.etag = etag
        # self.save()


class Region(EVEListedObject):
    LIST_URL = "https://esi.evetech.net/latest/universe/regions/"
    LIST_PAGED = False
    DETAILS_URL = "https://esi.evetech.net/latest/universe/regions/{id}/"
    eve_id = models.IntegerField(unique=True)
    name = models.CharField(max_length=128, unique=True)

    def __init__(self, *args, **kwargs):
        super(Region, self).__init__(*args, **kwargs)

    def __str__(self):
        return self.name

    def __repr__(self):
        return str(self)

    def load_dict(self, data: dict, etag: str):
        self.eve_id = data['region_id']
        self.name = data['name']
        self.etag = etag


class Constellation(EVEListedObject):
    LIST_URL = "https://esi.evetech.net/latest/universe/constellations/"
    LIST_PAGED = False
    DETAILS_URL = "https://esi.evetech.net/latest/universe/constellations/{id}/"
    eve_id = models.IntegerField(unique=True)
    name = models.CharField(max_length=128, unique=True)
    region_eve_id = models.IntegerField(null=True, db_index=True)

    def __init__(self, *args, **kwargs):
        super(Constellation, self).__init__(*args, **kwargs)

    def __str__(self):
        return self.name

    def __repr__(self):
        return str(self)

    def load_dict(self, data: dict, etag: str):
        self.eve_id = data['constellation_id']
        self.region_eve_id = data['region_id']
        self.name = data['name']
        self.etag = etag


class System(EVEListedObject):
    LIST_URL = "https://esi.evetech.net/latest/universe/systems/"
    LIST_PAGED = False
    DETAILS_URL = "https://esi.evetech.net/latest/universe/systems/{id}/"
    eve_id = models.IntegerField(unique=True)
    name = models.CharField(max_length=128, unique=True)
    constellation_eve_id = models.IntegerField(null=True, db_index=True)
    pos_x = models.FloatField(null=True)
    pos_y = models.FloatField(null=True)
    pos_z = models.FloatField(null=True)
    security = models.FloatField(null=True)

    def __init__(self, *args, **kwargs):
        super(System, self).__init__(*args, **kwargs)

    def __str__(self):
        return self.name

    def __repr__(self):
        return str(self)

    def load_dict(self, data: dict, etag: str):
        self.eve_id = data['system_id']
        self.constellation_eve_id = data['constellation_id']
        self.name = data['name']
        self.pos_x = data['position']['x']
        self.pos_y = data['position']['y']
        self.pos_z = data['position']['z']
        self.security = data['security_status']
        self.etag = etag


class SystemStats(models.Model):
    JUMPS = "https://esi.evetech.net/latest/universe/system_jumps/"
    KILLS = "https://esi.evetech.net/latest/universe/system_kills/"

    timestamp = models.DateTimeField(db_index=True)
    system_eve_id = models.PositiveBigIntegerField(db_index=True)
    jumps = models.PositiveIntegerField()
    npc_kills = models.PositiveIntegerField()
    pod_kills = models.PositiveIntegerField()
    ship_kills = models.PositiveIntegerField()


class MarketOrder(models.Model):
    REGION_ORDERS = "https://esi.evetech.net/latest/markets/{region_id}/orders/"
    STRUCTURE_ORDERS = "https://esi.evetech.net/latest/markets/structures/{structure_id}/"

    timestamp = models.DateTimeField(db_index=True, auto_now=True)
    eve_id = models.PositiveBigIntegerField(null=True, unique=True)
    duration = models.IntegerField()
    is_buy_order = models.BooleanField()
    issued = models.DateTimeField()
    location_eve_id = models.PositiveBigIntegerField(db_index=True)
    min_volume = models.IntegerField()
    price = models.FloatField()
    range = models.CharField(max_length=16, null=True)
    system_eve_id = models.IntegerField(null=True, db_index=True)
    type_eve_id = models.IntegerField(db_index=True)
    volume_remain = models.IntegerField()
    volume_total = models.IntegerField()
    region_eve_id = models.IntegerField(db_index=True, null=True)

    def load_dict(self, data: dict):
        self.eve_id = data['order_id']
        self.duration = data['duration']
        self.is_buy_order = data['is_buy_order']
        self.issued = data['issued']
        self.location_eve_id = data['location_id']
        self.min_volume = data['min_volume']
        self.price = data['price']
        self.range = data['range']
        self.system_eve_id = data.get('system_id', None)
        self.type_eve_id = data['type_id']
        self.volume_remain = data['volume_remain']
        self.volume_total = data['volume_total']

    @classmethod
    def reconcile(cls):
        pass
        # with connection.cursor() as cursor:
        #     cursor.execute("""
        #         UPDATE market_marketorder SET system_eve_id = ml.solar_system_eve_id
        #         FROM market_location ml
        #         WHERE system_eve_id IS NULL AND ml.eve_id = location_eve_id;""")
        #     cursor.execute("""\
        #         UPDATE market_marketorder SET region_eve_id = mc.region_eve_id
        #         FROM market_location ml
        #         JOIN market_system ms ON ml.solar_system_eve_id = ms.eve_id
        #         JOIN market_constellation mc ON mc.eve_id = ms.constellation_eve_id
        #         WHERE market_marketorder.region_eve_id IS NULL
        #             AND ml.eve_id = location_eve_id;""")

    @staticmethod
    def unique_by_field(collection, key='eve_id') -> list:
        history = set()
        for item in collection:
            if getattr(item, key) in history:
                continue
            history.add(getattr(item, key))
            yield item

    @classmethod
    def bulk_update_orders(cls, fresh_orders: list):
        incoming_ids = {o.eve_id for o in fresh_orders}
        known_orders = {o['eve_id']: o['id'] for o in MarketOrder.objects.filter(
            eve_id__in=incoming_ids).values('eve_id', 'id')}
        new = []
        for order in cls.unique_by_field(fresh_orders):
            if order.eve_id not in known_orders:
                new.append(order)
                continue
            order.id = known_orders[order.eve_id]
            del known_orders[order.eve_id]
            order.save()
        # MarketOrder.objects.filter(eve_id__in=known_orders.keys()).update(volume_remain=0)
        MarketOrder.objects.bulk_create(new, batch_size=500)

    @classmethod
    def update_current_prices_view(cls):
        """Refresh the materialized view: current_prices."""
        cls.objects.raw("REFRESH MATERIALIZED VIEW current_prices")


class Location(models.Model):
    STATION_URL = "https://esi.evetech.net/latest/universe/stations/{location_id}/"
    STRUCTURE_URL = "https://esi.evetech.net/latest/universe/structures/{location_id}/"
    MIN_STRUCTURE_ID = 100000000

    eve_id = models.PositiveBigIntegerField(unique=True)
    name = models.CharField(max_length=256, db_index=True)
    owner_id = models.IntegerField(null=True)
    solar_system_eve_id = models.PositiveIntegerField(null=True)
    type_eve_id = models.IntegerField(null=True)
    is_station = models.BooleanField()
    etag = models.CharField(max_length=64, null=True)
    readable = models.BooleanField(default=True)

    @classmethod
    def download_location(cls, eve_id):
        client = Client.get_client()
        if eve_id <= cls.MIN_STRUCTURE_ID:
            url = cls.STATION_URL
        else:
            url = cls.STRUCTURE_URL

        try:
            location = cls.objects.get(eve_id=eve_id)
        except cls.DoesNotExist:
            location = cls()

        res = client.make_request(url.format(location_id=eve_id), etag=location.etag)

        if res == Client.Http304:
            return
        elif res == Client.Http403:
            location.name = "Unknown {}".format(eve_id)
            location.eve_id = eve_id
            location.readable = False
            location.is_station = eve_id <= cls.MIN_STRUCTURE_ID
            location.save()
            return location

        data = res.json()
        location.eve_id = eve_id
        location.name = data['name']
        location.etag = res.headers['etag']
        location.type_eve_id = data['type_id']
        location.readable = True

        location.is_station = eve_id <= cls.MIN_STRUCTURE_ID
        if location.is_station:
            location.solar_system_eve_id = data['system_id']
            location.owner_id = data['owner']
        else:
            location.solar_system_eve_id = data['solar_system_id']
            location.owner_id = data['owner_id']
        location.save()
        return location


class MarketHistory(models.Model):
    HISTORY_URL = "https://esi.evetech.net/latest/markets/{region_id}/history/"

    region_eve_id = models.PositiveIntegerField(db_index=True)
    type_eve_id = models.PositiveIntegerField(db_index=True)
    date = models.DateField(db_index=True)
    highest = models.PositiveBigIntegerField()
    lowest = models.PositiveBigIntegerField()
    average = models.PositiveBigIntegerField()
    order_count = models.PositiveBigIntegerField()
    volume = models.PositiveBigIntegerField()

    class Meta:
        constraints = [
            UniqueConstraint(fields=['region_eve_id', 'type_eve_id', 'date'],
                             name='hisotry_uniqueness')
        ]

    @classmethod
    def download_data(cls, region_eve_id, item_eve_id: int):
        client = Client.get_client()
        url = cls.HISTORY_URL.format(region_id=region_eve_id)

        res = client.make_request(url, {'type_id': item_eve_id})

        if res == Client.Http404:
            print("Marking {} as not on market.".format(item_eve_id))
            Item.objects.filter(eve_id=item_eve_id).update(on_market=False)
            return []

        known_dates = {
            d.strftime("%Y-%m-%d")
            for d in MarketHistory.objects.filter(
                region_eve_id=region_eve_id,
                type_eve_id=item_eve_id).values_list('date', flat=True)}
        acc = []

        for day in res.json():
            if day['date'] in known_dates:
                continue
            mh = MarketHistory()
            mh.region_eve_id = region_eve_id
            mh.date = datetime.datetime.strptime(day['date'], "%Y-%m-%d")
            mh.type_eve_id = item_eve_id
            mh.highest = day['highest']
            mh.lowest = day['lowest']
            mh.average = day['average']
            mh.order_count = day['order_count']
            mh.volume = day['volume']
            acc.append(mh)

        return acc

    @classmethod
    def update_week_prices_view(cls):
        """Refresh the materialized view: week_prices."""
        cls.objects.raw("REFRESH MATERIALIZED VIEW week_prices")
