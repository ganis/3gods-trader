
from eve_auth.client import Client
from market.models import EVEListedObject

from multiprocessing import Process, Queue
from queue import Empty

class EVEObjectDownloader(Process):
    def __init__(self, job_queue: Queue, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.job_queue = job_queue

    def run(self) -> None:
        try:
            while True:
                job = self.job_queue.get_nowait()
                type_class, eve_id, etag = job
                assert(issubclass(type_class, EVEListedObject))
                type_class.update_eve_id(eve_id, etag)
        except Empty:
            return

