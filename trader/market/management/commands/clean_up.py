from datetime import timedelta

from django.core.management import BaseCommand
from django.db import connection, transaction
from django.utils import timezone

from market.models import MarketOrder


class Command(BaseCommand):
    def handle(self, *args, **options):
        week_ago = timezone.now() - timedelta(days=7)
        with transaction.atomic():
            MarketOrder.objects.filter(timestamp__lt=week_ago).delete()
            with connection.cursor() as cursor:
                cursor.execute("""
                DELETE from market_marketorder mo
                using (select mo1.eve_id, max(mo1.timestamp) as "timestamp" FROM market_marketorder mo1 group by eve_id) mo2
                WHERE mo.timestamp < mo2.timestamp and mo2.eve_id = mo.eve_id;
                """)