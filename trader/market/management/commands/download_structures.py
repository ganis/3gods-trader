"""
Downloads market orders for structures in given region.
This gets the orders from PRIVATE stations.
"""
from datetime import datetime
from multiprocessing import cpu_count
from multiprocessing.pool import ThreadPool

import tqdm
from django.core.management import BaseCommand
from django.db import transaction
from django.utils import timezone

from eve_auth import authenticate
from eve_auth.client import Client
from market.models import Region, MarketOrder, Location, Constellation, System


def unique_by_field(collection, key='eve_id') -> list:
    history = set()
    for item in collection:
        if getattr(item, key) in history:
            continue
        history.add(getattr(item, key))
        yield item


def process_structure_job(arg):
    location_eve_id, region_eve_id = arg
    client = Client.get_client()
    url = MarketOrder.STRUCTURE_ORDERS.format(structure_id=location_eve_id)
    first_call = client.make_request(url, only_headers=True)
    if first_call == Client.Http403:
        location = Location.objects.get(eve_id=location_eve_id)
        print("Marking location '{}' as unreadable.".format(location.name))
        location.readable = False
        location.save()
        return []
    pages = int(first_call.headers.get('X-Pages', '1'))
    orders = []
    for page in range(1, pages+1):
        res = client.make_request(url, page=page)
        if res == Client.Http403:
            location = Location.objects.get(eve_id=location_eve_id)
            print("Marking location '{}' as unreadable.".format(location.name))
            location.readable = False
            location.save()
            return []
        response = res.json()
        if not response:
            break
        for order_data in response:
            order = MarketOrder()
            order.load_dict(order_data)
            order.region_eve_id = region_eve_id
            orders.append(order)
        page += 1
    return orders


class Command(BaseCommand):
    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('region', type=str, )

    def download_structure_orders(self, region: Region, timestamp: datetime):
        constellation_ids = Constellation.objects.filter(region_eve_id=region.eve_id).values_list('eve_id', flat=True)
        solar_systems_ids = System.objects.filter(constellation_eve_id__in=constellation_ids).values_list('eve_id', flat=True)
        structure_by_ids = {loc.eve_id: loc for loc in Location.objects.filter(is_station=False, solar_system_eve_id__in=solar_systems_ids, readable=True)}
        workers = cpu_count() * 4
        with ThreadPool(workers) as tpool:
            with tqdm.tqdm(total=len(structure_by_ids)) as pbar:
                pbar.set_description('Downloading stations orders for {}...'.format(region))
                job_args = [(sid, region.eve_id) for sid in structure_by_ids.keys()]
                res = tpool.imap_unordered(process_structure_job, job_args)
                acc = []
                for orders in res:
                    acc.extend(orders)
                    pbar.update(1)
                for order in acc:
                    order.timestamp = timestamp
                    order.system_eve_id = structure_by_ids[order.location_eve_id].solar_system_eve_id
                    order.newest = True

        with transaction.atomic():
            old_orders = set(MarketOrder.objects.filter(location_eve_id__in=structure_by_ids.keys()).values_list('eve_id', flat=True))
            new_orders = {o.eve_id for o in acc}
            MarketOrder.objects.filter(eve_id__in=old_orders.difference(new_orders)).delete()
            MarketOrder.bulk_update_orders(acc)

    def handle(self, *args, **options):
        region_name = options['region']
        regions = []
        if region_name == 'all':
            regions = Region.objects.all()
        else:
            try:
                region = Region.objects.get(name__iexact=region_name)
                regions.append(region)
            except Region.DoesNotExist:
                all_regions = ", ".join(Region.objects.values_list('name', flat=True))
                self.stdout.write("Sorry, region not found. Known regions: {}".format(all_regions))
                return

        authenticate()

        timestamp = timezone.now()

        for region in regions:
            self.download_structure_orders(region, timestamp)

        print("Filling missing data in orders.")
        MarketOrder.reconcile()
        print("Refreshing the current_prices materialized view...")
        MarketOrder.update_current_prices_view()
        print("Done.")
