"""
Downloads information about a given Region (can be all).
This will download market orders for given region + any location info available.
This will download only orders from public stations.
"""
from datetime import datetime
from multiprocessing import cpu_count
from multiprocessing.pool import ThreadPool

import tqdm
from django.core.management import BaseCommand
from django.db import transaction
from django.utils import timezone

from eve_auth import authenticate
from eve_auth.client import Client
from market.models import Region, MarketOrder, Location, Constellation, System


def unique_by_field(collection, key='eve_id') -> list:
    history = set()
    for item in collection:
        if getattr(item, key) in history:
            continue
        history.add(getattr(item, key))
        yield item


def process_region_job(params_tuple):
    region_eve_id, page = params_tuple
    client = Client.get_client()
    res = client.make_request(MarketOrder.REGION_ORDERS.format(region_id=region_eve_id), page=page)
    orders = []
    for order_data in res.json():
        order = MarketOrder()
        order.load_dict(order_data)
        orders.append(order)
    return orders


class Command(BaseCommand):
    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('region', type=str, )
        parser.add_argument('--no-refresh', action='store_true')

    def download_locations_info(self, location_eve_ids):
        known_ids = Location.objects.filter(readable=True).values_list('eve_id', flat=True)
        location_eve_ids = set(location_eve_ids).difference(known_ids)
        workers = cpu_count() * 4
        with ThreadPool(workers) as tpool:
            with tqdm.tqdm(total=len(location_eve_ids)) as pbar:
                pbar.set_description('Downloading stations data...')
                res = tpool.imap_unordered(Location.download_location, location_eve_ids)
                for _ in res:
                    pbar.update(1)

    def download_region(self, region: Region, timestamp: datetime):
        print('Downloading orders for {}...'.format(region.name))
        client = Client.get_client()
        res = client.make_request(MarketOrder.REGION_ORDERS.format(region_id=region.eve_id),
                                  only_headers=True)
        page_count = int(res.headers['x-pages'])
        workers = cpu_count() * 4
        jobs = [(region.eve_id, page) for page in range(1, page_count + 1)]
        location_ids = set()
        with ThreadPool(workers) as tpool:
            with tqdm.tqdm(total=page_count) as pbar:
                pbar.set_description("Downloading {}".format(region.name))
                res = tpool.imap_unordered(process_region_job, jobs)
                acc = []
                for r in res:
                    pbar.update(1)
                    acc.extend(r)
                for order in acc:
                    order.timestamp = timestamp
                    order.region_eve_id = region.eve_id
                    order.newest = True
                    location_ids.add(order.location_eve_id)

                pbar.set_description("Saving {}".format(region.name))
        with transaction.atomic():
            old_orders = set(MarketOrder.objects.filter(
                region_eve_id=region.eve_id,
                location_eve_id__lt=Location.MIN_STRUCTURE_ID).values_list('eve_id', flat=True))
            # Only orders from public stations at this point
            acc = [o for o in acc if o.location_eve_id < Location.MIN_STRUCTURE_ID]
            new_orders = {o.eve_id for o in acc}
            MarketOrder.objects.filter(eve_id__in=old_orders.difference(new_orders)).delete()
            MarketOrder.bulk_update_orders(acc)

        self.download_locations_info(location_ids)

    def handle(self, *args, **options):
        region_name = options['region']
        regions = []
        if region_name == 'all':
            regions = Region.objects.all()
        else:
            try:
                region = Region.objects.get(name__iexact=region_name)
                regions.append(region)
            except Region.DoesNotExist:
                all_regions = ", ".join(Region.objects.values_list('name', flat=True))
                self.stdout.write("Sorry, region not found. Known regions: ", all_regions)
                return

        authenticate()

        timestamp = timezone.now()

        for region in regions:
            self.download_region(region, timestamp)

        print("Filling missing data in orders.")
        MarketOrder.reconcile()
        if not options['no_refresh']:
            print("Refreshing the current_prices materialized view...")
            MarketOrder.update_current_prices_view()
        print("Done.")
