"""
Downloads info about travel and killing stats in systems.
"""
from collections import defaultdict

from django.core.management import BaseCommand
from django.db.models import Max
from django.utils import timezone

from eve_auth import authenticate
from market.models import System, SystemStats
from eve_auth.client import Client
import pendulum

class Command(BaseCommand):
    def handle(self, *args, **options):
        authenticate()
        client = Client.get_client()
        print("Downloading stats...")
        res = client.make_request(SystemStats.JUMPS)
        res.raise_for_status()
        jumps_timestamp = pendulum.parse(res.headers['Last-Modified'], strict=False)
        jumps = res.json()
        res = client.make_request(SystemStats.KILLS)
        res.raise_for_status()
        kills_timestamp = pendulum.parse(res.headers['Last-Modified'], strict=False)
        kills = res.json()

        timestamp = max(jumps_timestamp, kills_timestamp)
        db_timestamp = SystemStats.objects.aggregate(Max('timestamp'))['timestamp__max']

        if db_timestamp and timestamp <= db_timestamp.astimezone(timestamp.timezone):
            print("No new data available.")
            return

        data = defaultdict(lambda: defaultdict(int))
        for pos in jumps:
            data[pos["system_id"]]['jumps'] = pos['ship_jumps']
        for pos in kills:
            data[pos["system_id"]].update(pos)

        acc = []
        for system_id, stats in data.items():
            sys_stat = SystemStats()
            sys_stat.system_eve_id = system_id
            sys_stat.timestamp = timestamp
            sys_stat.npc_kills = stats["npc_kills"]
            sys_stat.pod_kills = stats["pod_kills"]
            sys_stat.ship_kills = stats["ship_kills"]
            sys_stat.jumps = stats["jumps"]
            acc.append(sys_stat)
        SystemStats.objects.bulk_create(acc)