"""
Downloads the market history for given Region.
Market history shows how much and for how much an item group gets traded in region.
It does not show current market orders.
"""
from multiprocessing import cpu_count
from multiprocessing.pool import ThreadPool

import tqdm
from django.core.management import BaseCommand

from eve_auth import authenticate
from market.models import Region, MarketGroup, Item, MarketHistory

IGNORE_MARKET_GROUPS = [
    'Blueprints & Reactions',
    'Ship SKINs',
    'Apparel',
    'Special Edition Assets',
    'Skills',
    "Pilot's Services"
]


def history_job(args):
    region_eve_id, item_type_id = args
    return MarketHistory.download_data(region_eve_id, item_type_id)


class Command(BaseCommand):
    _MG_RECURSIVE_QUERY = """
    WITH RECURSIVE groups AS (
        SELECT *
        FROM market_marketgroup
        WHERE eve_id IN ({root_ids})
        UNION
        SELECT mg.*
        FROM market_marketgroup mg
        INNER JOIN groups g ON g.eve_id = mg.parent_group_eve_id
    ) SELECT * FROM groups;
    """

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('region', type=str, )
        parser.add_argument('--no-refresh', action='store_true')

    @classmethod
    def get_item_types(cls):
        root_market_groups = MarketGroup.objects.exclude(name__in=IGNORE_MARKET_GROUPS).filter(parent_group_eve_id__isnull=True).values_list('eve_id', flat=True)
        root_ids = ", ".join(str(mg) for mg in root_market_groups)
        groups = MarketGroup.objects.raw(cls._MG_RECURSIVE_QUERY.format(root_ids=root_ids))
        mg_ids = {mg.eve_id for mg in groups}

        return set(Item.objects.filter(market_group_eve_id__in=mg_ids, on_market=True).values_list('eve_id', flat=True))

    def handle(self, *args, **options):
        region_name = options['region']

        try:
            if region_name != 'all':
                regions = [Region.objects.get(name__iexact=region_name)]
            else:
                regions = Region.objects.all()
        except Region.DoesNotExist:
            all_regions = ", ".join(Region.objects.values_list('name', flat=True))
            self.stderr.write("Sorry, region not found. Known regions: ", all_regions)
            return

        authenticate()

        item_types_ids = self.get_item_types()
        workers = max(1, (cpu_count()-1)*4)

        for region in regions:
            jobs = [(region.eve_id, item_id) for item_id in item_types_ids]

            with ThreadPool(workers) as tpool:
                with tqdm.tqdm(total=len(jobs)) as pbar:
                    pbar.set_description("Downloading {} history".format(region.name))
                    res = tpool.imap_unordered(history_job, jobs)

                    for hist in res:
                        MarketHistory.objects.bulk_create(hist)
                        pbar.update(1)
            if not options['no_refresh']:
                print("Updating week_prices materialized view...")
                MarketHistory.update_week_prices_view()
        print('Done')
