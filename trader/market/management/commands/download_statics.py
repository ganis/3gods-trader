"""
Downloads static data from EVE API. Should be run before other commands when starting new DB and when there were
updates to the EVE world.
"""
from collections import defaultdict, namedtuple
from multiprocessing import cpu_count
from multiprocessing.pool import ThreadPool

import tqdm
from django.core.management import BaseCommand
from django.db import transaction

from eve_auth import authenticate
from market.models import EVEListedObject, MarketGroup, Item, Region, Constellation, System

DownloadJob = namedtuple('DownloadJob', ['type_class', 'eve_id', 'etag'])


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def process_job(job):
    type_class, eve_id, etag = job
    # print("Processing {}...".format(eve_id))
    assert (issubclass(type_class, EVEListedObject))
    ret = type_class.update_eve_id(eve_id, etag)
    # print("{} done.".format(eve_id))
    return ret


class Command(BaseCommand):
    CATEGORIES = {
        'types': Item,
        'market_groups': MarketGroup,
        'regions': Region,
        'constellations': Constellation,
        'systems': System
    }
    PROGRESS = 0

    def add_arguments(self, parser):
        pass
        # Positional arguments
        parser.add_argument('category', type=str, )
        #
        # # Named (optional) arguments
        # parser.add_argument(
        #     '--delete',
        #     action='store_true',
        #     help='Delete poll instead of closing it',
        # )

    def handle(self, *args, **options):
        type_class = options['category']
        try:
            type_class = self.CATEGORIES[type_class]
        except KeyError:
            self.stderr.write("Unknown category {}. Please pick one: {}".format(type_class, ", ".join(self.CATEGORIES.keys())))
            return
        etags = defaultdict(lambda: None)
        for eve_id, etag in type_class.get_list_of_known_ids():
            etags[eve_id] = etag

        job_queue = []
        authenticate()
        eve_types = type_class.get_list_of_ids()
        for eve_id in eve_types:
            job_queue.append(DownloadJob(type_class, eve_id, etags[eve_id]))
        workers = cpu_count() * 4

        new_items = []
        update_items = []

        with ThreadPool(workers) as tpool:
            with tqdm.tqdm(total=len(job_queue)) as pbar:
                res = tpool.imap_unordered(process_job, job_queue)
                for r in res:
                    pbar.update(1)
                    if hasattr(r, 'id') and r.id:
                        update_items.append(r)
                    elif r is not None:
                        new_items.append(r)
        print("Saving data to database...")
        with transaction.atomic():
            for r in update_items:
                r.save()
            type_class.objects.bulk_create(new_items, batch_size=500)
            type_class.reconcile()
        print('Done')

        # Load list of TypeIDs (EVEListedObject)
        # Load all TypeIDs (EVEListedObject)
        # Load all market groups (EVEListedObject)
        # Load all regions (EVEListedObject)
