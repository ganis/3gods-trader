# Generated by Django 3.1.2 on 2020-10-29 16:01

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('market', '0024_auto_20201018_1338'),
    ]

    operations = [
        migrations.AddField(
            model_name='marketorder',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
