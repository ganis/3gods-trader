# Generated by Django 3.1.2 on 2020-10-18 13:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('market', '0022_auto_20201018_1314'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='on_market',
            field=models.BooleanField(default=True),
        ),
    ]
