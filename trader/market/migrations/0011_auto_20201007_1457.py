# Generated by Django 3.1.2 on 2020-10-07 14:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('market', '0010_auto_20201007_1448'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='solar_system_id',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='location',
            name='type_eve_id',
            field=models.IntegerField(null=True),
        ),
    ]
