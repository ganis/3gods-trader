# Generated by Django 3.1.2 on 2020-10-09 09:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('market', '0012_auto_20201007_1502'),
    ]

    operations = [
        migrations.CreateModel(
            name='Constellation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('etag', models.CharField(max_length=64)),
                ('eve_id', models.IntegerField(unique=True)),
                ('name', models.CharField(max_length=128, unique=True)),
                ('region_eve_id', models.IntegerField(null=True)),
                ('region', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='market.region')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RenameField(
            model_name='marketorder',
            old_name='system_id',
            new_name='system_eve_id',
        ),
        migrations.AddField(
            model_name='marketorder',
            name='region',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='market.region'),
        ),
        migrations.AlterField(
            model_name='marketorder',
            name='region_eve_id',
            field=models.IntegerField(db_index=True, null=True),
        ),
        migrations.CreateModel(
            name='System',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('etag', models.CharField(max_length=64)),
                ('eve_id', models.IntegerField(unique=True)),
                ('name', models.CharField(max_length=128, unique=True)),
                ('constellation_eve_id', models.IntegerField(null=True)),
                ('constellation', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='market.constellation')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='marketorder',
            name='system',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='market.system'),
        ),
    ]
