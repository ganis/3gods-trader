# Generated by Django 3.1.2 on 2020-10-04 13:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('market', '0003_auto_20201003_1109'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='mass',
            field=models.FloatField(null=True),
        ),
        migrations.AlterField(
            model_name='item',
            name='volume',
            field=models.FloatField(null=True),
        ),
    ]
