# Generated by Django 3.1.2 on 2020-10-31 13:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('market', '0025_marketorder_timestamp'),
    ]

    operations = [
        migrations.RemoveConstraint(
            model_name='marketorder',
            name='unique_order',
        ),
        migrations.RemoveField(
            model_name='marketorder',
            name='date',
        ),
        migrations.RemoveField(
            model_name='marketorder',
            name='hour',
        ),
        migrations.AlterField(
            model_name='marketorder',
            name='timestamp',
            field=models.DateTimeField(db_index=True),
        ),
        migrations.AddConstraint(
            model_name='marketorder',
            constraint=models.UniqueConstraint(fields=('eve_id', 'timestamp'), name='unique_order'),
        ),
    ]
