import pprint
from copy import copy
from time import sleep

import requests
import multiprocessing
import multiprocessing.pool

_CLIENT_SINGLETON = None


def update_bearer_token(token: str):
    global _CLIENT_SINGLETON
    _CLIENT_SINGLETON = Client(token)


class Client:
    class Http304:
        #Unchanged
        pass

    class Http403:
        #Forbidden
        pass

    class Http404:
        #Not found
        pass

    class Http500:
        # Internal Server error
        pass

    def __init__(self, token: str):
        self.access_token = token
        self._token_header = "Bearer {}".format(token)
        self.headers = {
            'accept': 'application/json',
            'Authorization': self._token_header,
            'User-Agent': '3GODS Trader (author: Mestiv, mestiv@gmail.com)'
        }

    @staticmethod
    def _fetch(endpoint, params, headers):
        print('Fetching {}?{}'.format(endpoint, params))
        res = requests.get(endpoint, params, headers=headers)

        res.raise_for_status()
        return res.json()

    def get_page_count(self, endpoint: str):
        params = {
            'datasource': 'tranquility',
            'page': 1,
        }

        res = requests.head(endpoint, params=params, headers=self.headers)
        res.raise_for_status()

        return int(res.headers.get('X-Pages', '1'))

    def mass_requests(self, endpoint: str, params_: dict):
        params = {
            'datasource': 'tranquility',
            'page': 1,
        }

        params.update(params_)

        res = requests.get(endpoint, params, headers=self.headers)
        res.raise_for_status()

        page_count = int(res.headers.get('X-Pages', '1'))

        tasks = []

        for p in range(1, page_count+1):
            par_copy = copy(params)
            par_copy['page'] = p
            tasks.append([endpoint, par_copy, self.headers])
        pprint.pprint(tasks)

        with multiprocessing.pool.Pool(10) as pool:
            print('a')
            results = pool.starmap_async(self._fetch, tasks)
            print('b')
            pool.close()
            pool.join()
        acc = []

        for res in results.get():
            if res:
                acc.extend(res)

        return acc

    def make_request(self, endpoint: str, params_: dict = {}, page: int = 1, etag: str = None,
                     only_headers=False, silent_404=False):
        params = {
            'datasource': 'tranquility',
            'page': page,
        }

        params.update(params_)

        headers = copy(self.headers)
        if etag:
            headers['If-None-Match'] = etag
        tries = 0
        while tries < 3:
            if only_headers:
                res = requests.head(endpoint, params=params, headers=headers)
            else:
                res = requests.get(endpoint, params, headers=headers)
            if res.status_code == 304:
                return Client.Http304
            elif res.status_code == 403:
                print('Got forbidden error for {}, will sleep for 10s.'.format(endpoint))
                sleep(10)
                return Client.Http403
            elif res.status_code == 404:
                if not silent_404:
                    print(f'Request to {endpoint} with {params} failed with 404.')
                return Client.Http404
            elif res.status_code == 420:
                print('Got error 420, will sleep for {}.'.format(int(res.headers['X-Esi-Error-Limit-Reset'])))
                sleep(int(res.headers['X-Esi-Error-Limit-Reset']))
            elif res.status_code == 500:
                print(f'Request to {endpoint} with {params} failed with 500.')
                sleep(int(res.headers['X-Esi-Error-Limit-Reset']))
            elif res.status_code == 200:
                break
            else:
                pass
            tries += 1
            sleep(0.1)
        res.raise_for_status()
        return res

    @staticmethod
    def get_client() -> "Client":
        if _CLIENT_SINGLETON:
            return _CLIENT_SINGLETON
        raise RuntimeError("You want to use EVE Client without authenticating first!")

