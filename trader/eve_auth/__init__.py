""" Python 3 native (desktop/mobile) OAuth 2.0 example.
This example can be run from the command line and will show you how the
OAuth 2.0 flow should be handled if you are a web based application.
Prerequisites:
    * Create an SSO application at developers.eveonline.com with the scope
      "esi-characters.read_blueprints.v1" and the callback URL
      "https://localhost/callback/". Note: never use localhost as a callback
      in released applications.
    * Have a Python 3 environment available to you (possibly by using a
      virtual environment: https://virtualenv.pypa.io/en/stable/).
    * Run pip install -r requirements.txt with this directory as your root.
To run this example, make sure you have completed the prerequisites and then
run the following command from this directory as the root:
>>> python esi_oauth_native.py
then follow the prompts.
"""
import base64
import hashlib
import secrets
import requests
import pathlib

from .shared_flow import print_auth_url
from .shared_flow import send_token_request
from .shared_flow import handle_sso_token_response
from . import client

SCOPES = (
    'publicData',
    'esi-universe.read_structures.v1',
    'esi-markets.structure_markets.v1',
    'esi-corporations.read_structures.v1',
    'esi-markets.read_character_orders.v1',
    'esi-markets.read_corporation_orders.v1'
)
CLIENT_ID = "795b59b4595b49a7b130d5d17729f271"


def get_refresh_token() -> (str, str):
    """
    Go through the OAuth authentication flow to obtain the refresh_token for user.

    :return:
    The access_token and refresh_token.
    """
    random = base64.urlsafe_b64encode(secrets.token_bytes(32))
    m = hashlib.sha256()
    m.update(random)
    d = m.digest()
    code_challenge = base64.urlsafe_b64encode(d).decode().replace("=", "")

    print_auth_url(CLIENT_ID, code_challenge=code_challenge, scopes=SCOPES)

    auth_code = input("Copy the \"code\" query parameter and enter it here: ")

    code_verifier = random

    form_values = {
        "grant_type": "authorization_code",
        "client_id": CLIENT_ID,
        "code": auth_code,
        "code_verifier": code_verifier
    }

    res = send_token_request(form_values)

    if res.status_code == 200:
        refresh_token = res.json()['refresh_token']
        access_token = res.json()['access_token']
        return access_token, refresh_token
    else:
        res.raise_for_status()


def get_new_bearer_token(refresh_token) -> (str, str):
    """
    Uses a refresh token to acquire new authorization token.
    :return:
    The new bearer token and refresh token.
    """
    params = {
        'grant_type': 'refresh_token',
        'refresh_token': refresh_token,
        'client_id': CLIENT_ID,
    }

    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Host': 'login.eveonline.com'
    }

    res = requests.post('https://login.eveonline.com/v2/oauth/token', params, headers=headers)

    if res.status_code == 200:
        refresh_token = res.json()['refresh_token']
        access_token = res.json()['access_token']
        return access_token, refresh_token
    else:
        print("Your refresh token has been rejected. Will try to acquire a new one.")
        return get_refresh_token()


def authenticate():
    try:
        with open(pathlib.Path('~/.3gods/auth').expanduser()) as auth_file:
            refresh_token = auth_file.read()
    except FileNotFoundError:
        access_token, refresh_token = get_refresh_token()
    else:
        access_token, refresh_token = get_new_bearer_token(refresh_token)
    pathlib.Path('~/.3gods').expanduser().mkdir(parents=True, exist_ok=True)
    with open(pathlib.Path('~/.3gods/auth').expanduser(), mode='w') as auth_file:
        auth_file.write(refresh_token)

    client.update_bearer_token(access_token)

    return access_token
