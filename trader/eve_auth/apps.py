from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'eve_auth'
